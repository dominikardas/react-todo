import React from 'react';

import TodosStore from 'components/Todos/TodosStore';
import TodosView from 'components/Todos/TodosView';

import './App.scss';

export default class App extends React.Component {
    render () {
        return (
            <div className="xApp">
                <div className="xViewWrapper">
                    <TodosView todosStore={TodosStore} />
                </div>
            </div>
        );
    }
}
