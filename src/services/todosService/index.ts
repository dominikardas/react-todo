import { Todo } from 'components/Todos/todos.interface';

class TodosService {

    async getTodos(): Promise<Todo[]> {

        const todos: Todo[] = [];
        let i: number = 0;

        let res = await fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json());

        res.map((todo: Todo) => {

            if (i == 10) return;

            todos.push({
                id: todo.id,
                title: todo.title,
                completed: todo.completed
            });

            i++;
        });

        return todos;
    }

    async addEditTodo(data: Todo): Promise<Todo> {

        const method: string    = (data.id ? 'PUT' : 'POST');
        const urlParams: string = (data.id ? '/' + data.id : '');

        const res = await fetch('https://jsonplaceholder.typicode.com/todos' + urlParams, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        })
            .then(res => res.json());

        let todo: Todo = {
            id: res.id,
            title: res.title,
            completed: res.completed
        }

        return todo;
    }

    async completeTodo(id: number, completed: boolean): Promise<boolean> {

        const res = await fetch('https://jsonplaceholder.typicode.com/todos/' + id, {
            method: 'PUT',
            body: JSON.stringify({
                id,
                completed,
            }),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            }
        });

        return !!res;
    }

    async deleteTodo(id: number): Promise<boolean> {

        const res = await fetch('https://jsonplaceholder.typicode.com/todos/' + id, {
            method: 'DELETE',
        });

        return !!res;
    }
}

export default new TodosService();
