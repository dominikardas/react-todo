import React from 'react';

import { inject, observer } from 'mobx-react';

import { Todo } from './todos.interface';

import './TodosListItem.scss';


interface IProps {

    todo: Todo,

    index: number,

    completeTodo:   (id: number) => void,
    setEditTodo:    (id: number) => void,
    deleteTodo:     (id: number) => void,
}

@inject('todo')
@observer
export default class TodosListItem extends React.PureComponent<IProps> {
    render() {

        const {todo, index, completeTodo, setEditTodo, deleteTodo} = this.props;

        const renderTodoTitle = () => {
            return (
                <h2>
                    {(index + 1) + '. ' + todo.title}
                </h2>
            );
        };

        const renderTodoActions = () => {
            return (
                <div className="xTodosListItem__actions">
                    <a onClick={() => completeTodo(todo.id ?? 0)}>
                        <input type="checkbox" checked={todo.completed ? true : false} readOnly />
                    </a>
                    <a onClick={() => setEditTodo(todo.id ?? 0)}>
                        Edit
                    </a>
                    <a onClick={() => deleteTodo(todo.id ?? 0)}>
                        Delete
                    </a>
                </div>
            );
        };


        return (
            <div className={`
                    xTodosListItem
                    ${this.props.todo.completed ? '-todos-completed' : ''}
                `}
            >
                {renderTodoTitle()}
                {renderTodoActions()}
            </div>
        );
    }
}
