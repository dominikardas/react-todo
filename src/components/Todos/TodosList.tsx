import React from 'react';

import { inject, observer } from 'mobx-react';

import { Todo } from './todos.interface';

import TodosListItem from './TodosListItem';

import './TodosList.scss';


interface IProps {

    isLoading: boolean,

    todos: Array<Todo>,

    completeTodo:   (id: number) => void,
    setEditTodo:    (id: number) => void,
    deleteTodo:     (id: number) => void,
}

@inject('todos')
@observer
export default class TodosList extends React.PureComponent<IProps> {

    render() {

        const {isLoading, todos, completeTodo, setEditTodo, deleteTodo} = this.props;

        const renderTodoItems = () => {
            return (
                todos.map((todo, index) => (
                    <TodosListItem
                        index={index}
                        key={todo.id}
                        todo={todo}
                        completeTodo={completeTodo}
                        setEditTodo={setEditTodo}
                        deleteTodo={deleteTodo}
                    />
                ))
            );
        };

        const renderNoTodos = () => {
            return (
                !isLoading
                &&
                <h2>No todos yet!<br />Use the form above to add one to your list.</h2>
            );
        }

        return (
            <div className="xTodosList">
                {
                    todos.length > 0
                    ? renderTodoItems()
                    : renderNoTodos()
                }
            </div>
        );
    }
}
