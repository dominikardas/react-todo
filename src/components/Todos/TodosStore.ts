import React from 'react';

import { makeAutoObservable } from 'mobx';

import { Todo } from './todos.interface';
import TodosService from 'services/todosService';


class TodosStore {

    isLoading: boolean = false;
    todos: Todo[] = [];

    addEditTodoTitle: string = '';
    isEditingTodo: boolean = false;
    editTodoId: number = 0;

    constructor() {
        makeAutoObservable(this, {}, { autoBind: true });
    }

    setLoading(loading: boolean) {
        this.isLoading = loading;
    }

    async setTodos() {

        this.setLoading(true);

        try {
            const todos = await TodosService.getTodos();
            this.todos = todos;
        } catch (e) {
        } finally {
            this.setLoading(false);
        }
    }

    validateAddEditTodoForm(): boolean {

        if (this.addEditTodoTitle.trim() === '') {
            return false;
        }

        return true;
    }

    async addEditTodo() {

        this.setLoading(true);

        const valid = this.validateAddEditTodoForm();

        if (valid) {

            try {

                const data: Todo = {
                    title: this.addEditTodoTitle,
                    completed: false
                };

                const todo = await TodosService.addEditTodo(data);

                if (!this.isEditingTodo) {
                    this.todos.push(todo);
                } else {

                    let updatedTodos = this.todos.map(item => {
                        if (item.id === this.editTodoId) {
                            return {...item, title: todo.title}
                        }
                        return item;
                    });

                    this.todos = updatedTodos;
                    this.setEditTodo(0);
                }

                this.addEditTodoTitle = '';

            } catch(e) {
            } finally {
                this.setLoading(false);
            }

        } else {
            this.setLoading(false);
        }
    }

    setAddEditTodoTitle(e: React.FormEvent<HTMLInputElement>) {
        this.addEditTodoTitle = e.currentTarget.value.trim();
    }

    setEditTodo(id: number) {
        this.isEditingTodo = (id > 0);
        this.editTodoId = id;
    }

    async completeTodo(id: number) {

        this.setLoading(true);

        try {

            let todo: Todo = this.todos.filter((item: Todo) => item.id === id)[0];
            if (!todo) {
                return;
            }

            const res = await TodosService.completeTodo(id, todo.completed);
            if (res) {
                const updatedTodos: Todo[] = this.todos.map(todo => {
                    if (todo.id === id) {
                        return {...todo, completed: !todo.completed};
                    }
                    return todo;
                });

                this.todos = updatedTodos;
            }

        } catch(e) {
        } finally {
            this.setLoading(false);
        }
    }

    async deleteTodo(id: number) {

        this.setLoading(true);

        try {

            let todo: Todo = this.todos.filter((item: Todo) => item.id === id)[0];
            if (!todo) {
                return;
            }

            const res = await TodosService.deleteTodo(id);
            if (res) {
                const updatedTodos: Todo[] = this.todos.filter((todo) => todo.id !== id)
                this.todos = updatedTodos;
            }

        } catch (e) {
        } finally {
            this.setLoading(false);
        }
    }

    get incompleteTodos(): number {
        return (this.todos.filter(todo => todo.completed === false).length);
    }
}

export default new TodosStore();
