import React from 'react';

import { observer } from 'mobx-react';

import TodosStore from './TodosStore';

import TodosAddEdit from './TodosAddEdit';
import TodosList from './TodosList';

import LoadingOverlay from 'components/LoadingOverlay';

import './TodosView.scss';


type IProps = {
    todosStore: typeof TodosStore,
}

@observer
export default class TodosView extends React.PureComponent<IProps> {

    constructor(props: IProps) {
        super(props);
    }

    componentDidMount() {
        this.props.todosStore.setTodos();
    }

    render() {

        const {todosStore} = this.props;

        const incompleteTodosCount = () => {
            return (
                <p className="-todos-incomplete-count">
                    Todos to complete: {todosStore.incompleteTodos}
                </p>
            )
        };

        return (
          <div className="xTodosView">
              <div className="xTodosView__todos">
                  <h1>Todo List </h1>
                  {incompleteTodosCount()}
                  <TodosAddEdit
                        addEditTodo={todosStore.addEditTodo}
                        setEditTodo={todosStore.setEditTodo}
                        addEditTodoTitle={todosStore.addEditTodoTitle}
                        setAddEditTodoTitle={todosStore.setAddEditTodoTitle}
                        isEditingTodo={todosStore.isEditingTodo}
                        editTodoId={todosStore.editTodoId}
                  />
                  <TodosList
                        todos={todosStore.todos}
                        isLoading={todosStore.isLoading}
                        completeTodo={todosStore.completeTodo}
                        setEditTodo={todosStore.setEditTodo}
                        deleteTodo={todosStore.deleteTodo}
                  />
                  {todosStore.isLoading && <LoadingOverlay />}
              </div>
          </div>
        );
    }
}
