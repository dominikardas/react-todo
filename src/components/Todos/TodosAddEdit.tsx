import React from 'react';

import './TodosAddEdit.scss';


interface IProps {

    addEditTodo: () => void,
    setEditTodo: (id: number) => void,

    addEditTodoTitle: string,
    setAddEditTodoTitle: (e: React.FormEvent<HTMLInputElement>) => void,
    isEditingTodo: boolean,
    editTodoId: number,
}

export default class TodosAddEdit extends React.PureComponent<IProps> {

    submitAddEditForm(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        this.props.addEditTodo();
    }

    render() {

        const {setEditTodo, addEditTodoTitle, setAddEditTodoTitle, isEditingTodo, editTodoId} = this.props;

        const renderEditingWarning = () => {
            return (
                <p className="-todos-edit-warning">
                    Editing Todo with ID {editTodoId}
                    &nbsp;&nbsp;
                    <a onClick={() => setEditTodo(0)}>[Stop editing]</a>
                </p>
            );
        };

        const renderAddEditForm = () => {
            return (
                <form onSubmit={(e) => this.submitAddEditForm(e)}>
                    <input type="text" value={addEditTodoTitle} onChange={setAddEditTodoTitle} />
                    <button type="submit">{isEditingTodo ? 'Edit' : 'Add'} Todo</button>
                </form>
            );
        }

        return (
            <div
                className={`
                    xTodosAddEdit
                    ${isEditingTodo ? '-todos-form-editing' : ''}
                `}
            >
                {isEditingTodo && renderEditingWarning()}
                {renderAddEditForm()}
            </div>
        );
    }
}
