import React from 'react';

export default class LoadingOverlay extends React.Component {
    render() {
        return (
          <div className="xLoadingOverlay">
              <div className="lds-dual-ring"></div>
          </div>
        );
    }
}
